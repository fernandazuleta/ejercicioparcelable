package com.example.mariaquinonez.macapp.objects.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;

import com.example.mariaquinonez.macapp.R;
import com.example.mariaquinonez.macapp.objects.ejercicio2.McOptions;

/**
 * Created by maria.quinonez on 23/11/2017.
 */

public class McOrderHolder extends RecyclerView.ViewHolder {
    private CheckBox cbxOptions;
    public McOrderHolder(View itemView) {
        super(itemView);
        cbxOptions=itemView.findViewById(R.id.cbx_options);
    }

    public CheckBox getCbxOptions() {
        return cbxOptions;
    }

    public void setCbxOptions(CheckBox cbxOptions) {
        this.cbxOptions = cbxOptions;
    }
    public void setMcOption(McOptions mcOption){
        cbxOptions.setText(mcOption.getStrOptions());
    }
}
