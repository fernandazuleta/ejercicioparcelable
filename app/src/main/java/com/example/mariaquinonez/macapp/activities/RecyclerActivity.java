package com.example.mariaquinonez.macapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.example.mariaquinonez.macapp.R;
import com.example.mariaquinonez.macapp.objects.adapters.ItemRecyclerAdapter;
import com.example.mariaquinonez.macapp.objects.ejercicio2.FoodType;
import com.example.mariaquinonez.macapp.objects.ejercicio2.McOrden;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RecyclerActivity extends AppCompatActivity {
    private RecyclerView rvListOrder;
    private ItemRecyclerAdapter itemRecyclerAdapter;
    private final int COD_DETAIL_ORDER = 19;

    public static String RESULT_STR = "strResult";
    private final int NEW_ORDER_CODE = 344;
    private final int SAVED_STATE=12;
    private final String STATE="ESTADO";
    List<McOrden> result = new ArrayList<>();
    List<McOrden> mcOrderSaved= new ArrayList<>();

    @Override
    protected void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
        outState.putInt(STATE,SAVED_STATE);
    }
    @Override
    protected void onRestoreInstanceState(Bundle bundle){

        super.onRestoreInstanceState(bundle);

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //mercedes implementacion
                startActivityForResult(new Intent(RecyclerActivity.this
                        , AddMcOrderMecActivity.class), COD_DETAIL_ORDER);
                //mio startActivityForResult(new Intent(RecyclerActivity.this
                 //       , AddOrderActivity.class), COD_DETAIL_ORDER);
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                //      .setAction("Action", null).show();
            }
        });
        initComponents();


        if(savedInstanceState!=null){
            Log.i("SavedState@@@@@@@@@@", savedInstanceState.toString());
            itemRecyclerAdapter = new ItemRecyclerAdapter(mcOrderSaved, this, R.layout.item_line_order);
            Log.i("RecyclerActivity", "Set Adapter");
            rvListOrder.setAdapter(itemRecyclerAdapter);

        }
    }

    private void initComponents() {
        rvListOrder = (RecyclerView) findViewById(R.id.rv_list_order);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 1);
        Log.i("RecyclerActivity", "Set Grid Layout Manager");
        rvListOrder.setLayoutManager(gridLayoutManager);

        itemRecyclerAdapter = new ItemRecyclerAdapter(result, this, R.layout.item_line_order);
        Log.i("RecyclerActivity", "Set Adapter");
        rvListOrder.setAdapter(itemRecyclerAdapter);

    }

   /* private List<McOrden> getOrders() {


        McOrden mcTemp = new McOrden();
        mcTemp.setFoodType(FoodType.CAFE);
        mcTemp.setConList(Arrays.asList("leche", "azucar"));
        mcTemp.setSinList(Arrays.asList("Descafeinado"));
        result.add(mcTemp);

        mcTemp = new McOrden();
        mcTemp.setFoodType(FoodType.HAMBURGUESA);
        mcTemp.setConList(Arrays.asList("lechuga", "tomate"));
        mcTemp.setSinList(Arrays.asList("pepinillo"));
        result.add(mcTemp);

        mcTemp = new McOrden();
        mcTemp.setFoodType(FoodType.MC_FLURY);
        mcTemp.setConList(Arrays.asList("oreo", "m&m"));
        mcTemp.setSinList(Arrays.asList("caramelo"));
        result.add(mcTemp);

        return result;

    }*/


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if ((resultCode == RESULT_OK)
                && (data != null)) {
            //result
            result.add((McOrden) data.getParcelableExtra(RESULT_STR));
            itemRecyclerAdapter.notifyDataSetChanged();
            mcOrderSaved.addAll(result);
        }
    }
}
