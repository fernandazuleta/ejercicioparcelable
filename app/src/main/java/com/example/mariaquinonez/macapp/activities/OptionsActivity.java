package com.example.mariaquinonez.macapp.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.mariaquinonez.macapp.R;
import com.example.mariaquinonez.macapp.SplashScreenActivity;
import com.example.mariaquinonez.macapp.objects.Cafe;
import com.example.mariaquinonez.macapp.objects.Hamburguesa;
import com.example.mariaquinonez.macapp.objects.McFlurry;
import com.example.mariaquinonez.macapp.objects.Orden;
import com.example.mariaquinonez.macapp.objects.Orden2;

import java.util.ArrayList;
import java.util.List;

public class OptionsActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_options2);

        Orden orden = getIntent().getParcelableExtra(SplashScreenActivity.key);
        System.out.println(":" + orden);

        Toast.makeText(this,"Se pidio"+orden.getCanCafe()+"Cafes"+orden.getCanCafe()+"Hamburguesas"+orden.getCanHamburguesa()+"McFlurry"+orden.getCanMcFlurry()+"papas"+orden.getCanPapitas(),Toast.LENGTH_LONG).show();


    }
    //
    public void ejercicioParcelable(View view){

        List<Hamburguesa> hamburguesasList= new ArrayList<>();
        hamburguesasList.add(new Hamburguesa(true,false,true));

        List<Cafe> cafe= new ArrayList<>();
        cafe.add(new Cafe(true,false,true));

        List<McFlurry> mcFlurryList= new ArrayList<>();
        mcFlurryList.add(new McFlurry(true,false,true));

        Orden2 nuevaOrden= new Orden2(hamburguesasList,mcFlurryList, cafe);
        Intent intent=new Intent(this, EjercicioParcelableActivity.class);
        intent.putExtra("llave", nuevaOrden);
        startActivity(intent);
    }
    public void goToCicle(View view){
        startActivity(new Intent(this,CicleActivity.class));
    }

    public void goToCbx(View view) {
        startActivity(new Intent(this, CheckBoxActivity.class));
    }
    public void goToListView(View view){
        startActivity(new Intent(this, ListViewActivity.class));
    }
    public void goToResult(View view){startActivity(new Intent(this,ResultActivity.class));}
    public void goToRecyclerView(View view){startActivity(new Intent(this,RecyclerActivity.class));}

}
