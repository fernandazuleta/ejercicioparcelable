package com.example.mariaquinonez.macapp.objects;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by maria.quinonez on 26/09/2017.
 */

public class Orden implements Parcelable {
    private int canHamburguesa;
    private int canMcFlurry;
    private int canPapitas;
    private int canCafe;
    
    public Orden() {
    }

    public Orden(int canHamburguesa, int canMcFlurry, int canPapitas, int canCafe) {
        this.canHamburguesa = canHamburguesa;
        this.canMcFlurry = canMcFlurry;
        this.canPapitas = canPapitas;
        this.canCafe = canCafe;
    }

    protected Orden(Parcel in) {
        canHamburguesa = in.readInt();
        canMcFlurry = in.readInt();
        canPapitas = in.readInt();
        canCafe = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(canHamburguesa);
        dest.writeInt(canMcFlurry);
        dest.writeInt(canPapitas);
        dest.writeInt(canCafe);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Orden> CREATOR = new Parcelable.Creator<Orden>() {
        @Override
        public Orden createFromParcel(Parcel in) {
            return new Orden(in);
        }

        @Override
        public Orden[] newArray(int size) {
            return new Orden[size];
        }
    };

    public int getCanHamburguesa() {
        return canHamburguesa;
    }

    public void setCanHamburguesa(int canHamburguesa) {
        this.canHamburguesa = canHamburguesa;
    }

    public int getCanMcFlurry() {
        return canMcFlurry;
    }

    public void setCanMcFlurry(int canMcFlurry) {
        this.canMcFlurry = canMcFlurry;
    }

    public int getCanPapitas() {
        return canPapitas;
    }

    public void setCanPapitas(int canPapitas) {
        this.canPapitas = canPapitas;
    }

    public int getCanCafe() {
        return canCafe;
    }

    public void setCanCafe(int canCafe) {
        this.canCafe = canCafe;
    }
}