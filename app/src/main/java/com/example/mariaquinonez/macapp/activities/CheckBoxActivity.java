package com.example.mariaquinonez.macapp.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;

import com.example.mariaquinonez.macapp.R;

/**
 * Created by maria.quinonez on 26/10/2017.
 */

public class CheckBoxActivity  extends AppCompatActivity{

    private CheckBox cbxAtm;
    private CheckBox cbxBag;
    private CheckBox cbxBasket;
    private CheckBox cbxBox;
    private CheckBox cbxBriefcase;
    private CheckBox cbxCalculator;

    private ImageView ivAtm;
    private ImageView ivBag;
    private ImageView ivBasket;
    private ImageView ivBox;
    private ImageView ivBriefcase;
    private ImageView ivCalculator;


    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chek_box);
        initComponents();
    }

    private void initComponents(){
        cbxAtm=(CheckBox) findViewById(R.id.cbx_opt_atm);
        cbxAtm.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked){
                    ivAtm.setVisibility(View.VISIBLE);
                }else{
                    ivAtm.setVisibility(View.GONE);
                }
            }
        });
        cbxBag=(CheckBox) findViewById(R.id.cbx_opt_bag);
        cbxBag.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked){
                    ivBag.setVisibility(View.VISIBLE);
                }else{
                    ivBag.setVisibility(View.GONE);
                }
            }
        });
        cbxBasket=(CheckBox) findViewById(R.id.cbx_opt_basket);
        cbxBasket.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked){
                    ivBasket.setVisibility(View.VISIBLE);
                }else{
                    ivBasket.setVisibility(View.GONE);
                }
            }
        });
        cbxBox=(CheckBox) findViewById(R.id.cbx_opt_box);
        cbxBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked){
                    ivBox.setVisibility(View.VISIBLE);
                }else{
                    ivBox.setVisibility(View.GONE);
                }
            }
        });
        cbxBriefcase=(CheckBox) findViewById(R.id.cbx_opt_brief);
        cbxBriefcase.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked){
                    ivBriefcase.setVisibility(View.VISIBLE);
                }else{
                    ivBriefcase.setVisibility(View.GONE);
                }
            }
        });
        cbxCalculator=(CheckBox) findViewById(R.id.cbx_opt_cal);
        cbxCalculator.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked){
                    ivCalculator.setVisibility(View.VISIBLE);
                }else{
                    ivCalculator.setVisibility(View.GONE);
                }
            }
        });

        ivAtm=(ImageView) findViewById(R.id.iv_atm);
        ivBag=(ImageView) findViewById(R.id.iv_bag);
        ivBasket=(ImageView) findViewById(R.id.iv_basket);
        ivBox=(ImageView) findViewById(R.id.iv_box);
        ivBriefcase=(ImageView) findViewById(R.id.iv_brief);
        ivCalculator=(ImageView) findViewById(R.id.iv_cal);

    }

}
