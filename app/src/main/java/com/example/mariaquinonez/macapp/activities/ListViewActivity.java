package com.example.mariaquinonez.macapp.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.example.mariaquinonez.macapp.R;

import java.util.ArrayList;
import java.util.Arrays;


public class ListViewActivity extends AppCompatActivity {

    private ListView lvOpt;
    private EditText etElement;
    private ArrayAdapter<String> adapter;
    private ArrayList<String> listItem;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);
        initComponents();
    }
    private void initComponents(){
        listItem=new ArrayList<>();
        etElement= (EditText) findViewById(R.id.et_new_element);

        lvOpt= (ListView) findViewById(R.id.lv_opt);
        String[] options = getResources().getStringArray(R.array.img_opt);
        listItem.addAll(Arrays.asList(options));
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,listItem);
        lvOpt.setAdapter(adapter);
    }

    public void addNewElement(View view){
        Log.i("ListViewActiviry","llego a addNewElement@@@"+etElement.getText().toString());
        if (adapter!=null && etElement!=null){
            listItem.add(etElement.getText().toString());
            lvOpt.setAdapter(adapter);
            etElement.setText("");
        }
    }
}
