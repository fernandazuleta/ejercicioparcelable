package com.example.mariaquinonez.macapp.activities;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.mariaquinonez.macapp.R;

import java.util.ArrayList;

import static android.R.attr.data;

public class ResultActivity extends AppCompatActivity {
    private ListView lvOrders;
    private ArrayAdapter<String> orderAdapter;
    private final int NEW_ORDER_CODE=123;
    public static String RESULT_STR="strResult";
    private Fragment dataFragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                            startActivityForResult(new Intent(ResultActivity.this, NewOrderActivity.class),NEW_ORDER_CODE );
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                  //      .setAction("Action", null).show();
            }
        });

        initComponents();
    }
    private void initComponents(){
        lvOrders= (ListView) findViewById(R.id.lv_orders);
        orderAdapter= new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, new ArrayList<String>());
        lvOrders.setAdapter(orderAdapter);

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if((requestCode==NEW_ORDER_CODE) &&(resultCode==RESULT_OK)
                && (data!=null)){
            orderAdapter.add(data.getStringExtra(RESULT_STR));
        }
    }

}
