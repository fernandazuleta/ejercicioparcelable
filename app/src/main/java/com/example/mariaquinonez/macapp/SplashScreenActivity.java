package com.example.mariaquinonez.macapp;

import android.content.Intent;
import android.os.Handler;
import android.print.PageRange;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.mariaquinonez.macapp.activities.OptionsActivity;
import com.example.mariaquinonez.macapp.objects.Orden;

public class SplashScreenActivity extends AppCompatActivity {

    private Handler mHandler;
    public static final String key="llave";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        mHandler= new Handler();
        mHandler.postDelayed(runOptions,5_000);

    }

    private Runnable runOptions= new Runnable() {
        @Override
        public void run() {
            goToOptions();
        }
    };

    private void goToOptions(){
        Intent intent=new Intent(this, OptionsActivity.class);
        //
        Orden orden = new Orden(3,0,3,0);
        intent.putExtra(key,orden);

        startActivity(intent);
    }
}
