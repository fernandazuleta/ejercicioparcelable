package com.example.mariaquinonez.macapp.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.example.mariaquinonez.macapp.objects.ejercicio2.McOrden;

/**
 * Created by maria.quinonez on 27/11/2017.
 */

public class RetainedFragment extends Fragment {
    private McOrden data;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // retain this fragment
        setRetainInstance(true);
    }

    public void setData(McOrden data) {
        this.data = data;
    }

    public McOrden getData() {
        return data;
    }
}
