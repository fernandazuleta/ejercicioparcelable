package com.example.mariaquinonez.macapp.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;

import com.example.mariaquinonez.macapp.R;
import com.example.mariaquinonez.macapp.objects.adapters.McOrderAdapter;
import com.example.mariaquinonez.macapp.objects.ejercicio2.FoodType;
import com.example.mariaquinonez.macapp.objects.ejercicio2.McOptions;
import com.example.mariaquinonez.macapp.objects.ejercicio2.McOrden;

import java.util.ArrayList;
import java.util.List;

public class AddMcOrderMecActivity extends AppCompatActivity {

    private ImageView ivSelected;
    private Spinner sMcOrder;
    private RecyclerView rvOptions;
    private McOrderAdapter mcOrderAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_mc_order_mec);
        initComponents();

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu ){
        getMenuInflater().inflate(R.menu.add_mc_order_menu,menu);
        return true;
    }
     @Override
     public boolean onOptionsItemSelected (MenuItem item ){
         switch (item.getItemId()){
             case R.id.add_mc_order:
                 McOrden mcOrden= new McOrden();
                 mcOrden.setFoodType(FoodType.getFoodType(sMcOrder.getSelectedItemPosition()));
                 for(McOptions opt: mcOrderAdapter.getMcOptionsList()){
                     if(opt.isSelected()){
                         mcOrden.getConList().add(opt.getStrOptions());
                     }else{
                         mcOrden.getSinList().add(opt.getStrOptions());
                     }
                 }
                 Intent resultIntent= new Intent();
                 resultIntent.putExtra(RecyclerActivity.RESULT_STR,mcOrden);
                 setResult(RESULT_OK,resultIntent);
                 break;
         }

         finish();
         return super.onOptionsItemSelected(item);
     }


    private void  initComponents(){
        ivSelected= (ImageView) findViewById(R.id.iv_selected);
        sMcOrder= (Spinner) findViewById(R.id.s_mac_order);
        rvOptions= (RecyclerView) findViewById(R.id.rv_order_options);

        GridLayoutManager gridLayoutManager=new GridLayoutManager(this, 3);

        rvOptions.setLayoutManager(gridLayoutManager);

        sMcOrder.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,
                getResources().getStringArray(R.array.orden_opt)));

        sMcOrder.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                changeOptions();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        changeOptions();
    }



    private void changeOptions(){
        switch (sMcOrder.getSelectedItemPosition()){
            case 0:
                ivSelected.setImageResource(R.drawable.cafe);
                mcOrderAdapter= new McOrderAdapter(generateMcOpt(getResources().getStringArray(R.array.cafe_options)),this);
                break;
            case 1:
                ivSelected.setImageResource(R.drawable.mcflurry);
                mcOrderAdapter= new McOrderAdapter(generateMcOpt(getResources().getStringArray(R.array.mcFlurry_options)),this);
                break;
            case 2:
                ivSelected.setImageResource(R.drawable.hamburguer);
                mcOrderAdapter= new McOrderAdapter(generateMcOpt(getResources().getStringArray(R.array.hamburguesa_options)),this);
                break;
        }
        rvOptions.setAdapter(mcOrderAdapter);

    }
    private List<McOptions> generateMcOpt(String[] mcOpt){
        List<McOptions> result= new ArrayList<>();
        for(String strOpt: mcOpt){
            result.add(new McOptions(strOpt,false));
        }
        return result;
    }
}

//onResitreInstantState
//mejor hacerlo en el onCreate
//crear un objeto que tenga una lista para construirlo