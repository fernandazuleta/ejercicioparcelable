package com.example.mariaquinonez.macapp.objects;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by maria.quinonez on 10/10/2017.
 */

public class Orden2 implements Parcelable {
    private List<Hamburguesa> hamburguesaList;
    private List<McFlurry> mcFlurryList;
    private List<Cafe> cafeList;

    public Orden2() {
    }

    public Orden2(List<Hamburguesa> hamburguesaList, List<McFlurry> mcFlurryList, List<Cafe> cafeList) {
        this.hamburguesaList = hamburguesaList;
        this.mcFlurryList = mcFlurryList;
        this.cafeList = cafeList;
    }

    public List<Hamburguesa> getHamburguesaList() {
        return hamburguesaList;
    }

    public void setHamburguesaList(List<Hamburguesa> hamburguesaList) {
        this.hamburguesaList = hamburguesaList;
    }

    public List<McFlurry> getMcFlurryList() {
        return mcFlurryList;
    }

    public void setMcFlurryList(List<McFlurry> mcFlurryList) {
        this.mcFlurryList = mcFlurryList;
    }

    public List<Cafe> getCafeList() {
        return cafeList;
    }

    public void setCafeList(List<Cafe> cafeList) {
        this.cafeList = cafeList;
    }

    protected Orden2(Parcel in) {
        if (in.readByte() == 0x01) {
            hamburguesaList = new ArrayList<Hamburguesa>();
            in.readList(hamburguesaList, Hamburguesa.class.getClassLoader());
        } else {
            hamburguesaList = null;
        }
        if (in.readByte() == 0x01) {
            mcFlurryList = new ArrayList<McFlurry>();
            in.readList(mcFlurryList, McFlurry.class.getClassLoader());
        } else {
            mcFlurryList = null;
        }
        if (in.readByte() == 0x01) {
            cafeList = new ArrayList<Cafe>();
            in.readList(cafeList, Cafe.class.getClassLoader());
        } else {
            cafeList = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (hamburguesaList == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(hamburguesaList);
        }
        if (mcFlurryList == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(mcFlurryList);
        }
        if (cafeList == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(cafeList);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Orden2> CREATOR = new Parcelable.Creator<Orden2>() {
        @Override
        public Orden2 createFromParcel(Parcel in) {
            return new Orden2(in);
        }

        @Override
        public Orden2[] newArray(int size) {
            return new Orden2[size];
        }
    };
}