package com.example.mariaquinonez.macapp.objects.ejercicio2;

/**
 * Created by maria.quinonez on 23/11/2017.
 */

public class McOptions {
    private String strOptions;
    private boolean selected;


    public McOptions() {
    }

    public McOptions(String strOptions, boolean selected) {
        this.strOptions = strOptions;
        this.selected = selected;
    }

    public String getStrOptions() {
        return strOptions;
    }



    public void setStrOptions(String strOptions) {
        this.strOptions = strOptions;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

}
