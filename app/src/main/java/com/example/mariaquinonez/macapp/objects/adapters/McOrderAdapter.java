package com.example.mariaquinonez.macapp.objects.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.example.mariaquinonez.macapp.R;
import com.example.mariaquinonez.macapp.objects.ejercicio2.McOptions;
import com.example.mariaquinonez.macapp.objects.holders.McOrderHolder;

import java.util.List;

/**
 * Created by maria.quinonez on 23/11/2017.
 */

public class McOrderAdapter extends RecyclerView.Adapter<McOrderHolder> {
    private List<McOptions> mcOptionsList;
    private Activity activity;

    public McOrderAdapter(List<McOptions> mcOptionsList, Activity activity) {
        this.mcOptionsList = mcOptionsList;
        this.activity=activity;
    }

    @Override
    public McOrderHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new McOrderHolder(LayoutInflater.from(activity).inflate(R.layout.item_new_order,parent, false));
    }

    @Override
    public void onBindViewHolder(McOrderHolder holder, int position) {
        final McOptions mcOptions= mcOptionsList.get(position);
        holder.setMcOption(mcOptions);
        holder.getCbxOptions().setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                mcOptions.setSelected(b);
            }
        });
    }

    public List<McOptions> getMcOptionsList() {
        return mcOptionsList;
    }

    @Override
    public int getItemCount() {
        return mcOptionsList.size();
    }
}
