package com.example.mariaquinonez.macapp.objects.ejercicio2;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by maria.quinonez on 02/11/2017.
 */

public class McOrden implements Parcelable{
    private FoodType foodType;
    private List<String> conList;
    private List<String> sinList;

    public McOrden(){
        conList= new ArrayList<>();
        sinList=new ArrayList<>();
    }

    public FoodType getFoodType() {
        return foodType;
    }

    public void setFoodType(FoodType foodType) {
        this.foodType = foodType;
    }

    public List<String> getConList() {
        return conList;
    }

    public void setConList(List<String> conList) {
        this.conList = conList;
    }

    public List<String> getSinList() {
        return sinList;
    }

    public void setSinList(List<String> sinList) {
        this.sinList = sinList;
    }
    protected McOrden(Parcel in) {
        foodType = (FoodType) in.readValue(FoodType.class.getClassLoader());
        if (in.readByte() == 0x01) {
            conList = new ArrayList<String>();
            in.readList(conList, String.class.getClassLoader());
        } else {
            conList = null;
        }
        if (in.readByte() == 0x01) {
            sinList = new ArrayList<String>();
            in.readList(sinList, String.class.getClassLoader());
        } else {
            sinList = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(foodType);
        if (conList == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(conList);
        }
        if (sinList == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(sinList);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<McOrden> CREATOR = new Parcelable.Creator<McOrden>() {
        @Override
        public McOrden createFromParcel(Parcel in) {
            return new McOrden(in);
        }

        @Override
        public McOrden[] newArray(int size) {
            return new McOrden[size];
        }
    };
}

