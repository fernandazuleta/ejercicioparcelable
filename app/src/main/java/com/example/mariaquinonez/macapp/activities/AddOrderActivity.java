package com.example.mariaquinonez.macapp.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Spinner;

import com.example.mariaquinonez.macapp.R;
import com.example.mariaquinonez.macapp.objects.McFlurry;
import com.example.mariaquinonez.macapp.objects.ejercicio2.FoodType;
import com.example.mariaquinonez.macapp.objects.ejercicio2.McOrden;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AddOrderActivity extends AppCompatActivity {
    private Spinner sDetailOptions;
    private ArrayList<String> lstCon = new ArrayList<>();
    private ArrayList<String> lstSin = new ArrayList<>();

    private CheckBox cbxOreo;
    private CheckBox cbxMym;
    private CheckBox cbxTop;
    private CheckBox cbxCebolla;
    private CheckBox cbxTomate;
    private CheckBox cbxPepinillo;
    private CheckBox cbxLeche;
    private CheckBox cbxCafeina;
    private CheckBox cbxAzucar;
    McOrden mcTemp = new McOrden();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_order);
        initComponents();
    }

    private void initComponents() {

        sDetailOptions = (Spinner) findViewById(R.id.o_options);
        String[] options = {"McFlurry", "Cafe", "Hamburguesa"};
        cbxOreo = (CheckBox) findViewById(R.id.cbx_oreo);
        cbxMym = (CheckBox) findViewById(R.id.cbx_mym);
        cbxTop = (CheckBox) findViewById(R.id.cbx_toping);
        cbxTomate = (CheckBox) findViewById(R.id.cbx_tomate);
        cbxPepinillo = (CheckBox) findViewById(R.id.cbx_pepinillo);
        cbxLeche = (CheckBox) findViewById(R.id.cbx_leche);
        cbxCafeina = (CheckBox) findViewById(R.id.cbx_cafeina);
        cbxAzucar = (CheckBox) findViewById(R.id.cbx_azucar);

        cbxCebolla = (CheckBox) findViewById(R.id.cbx_cebolla);

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, options);

        sDetailOptions.setAdapter(adapter);
        sDetailOptions.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {

                switch (position) {
                    case 0:

                        cbxOreo.setVisibility(View.VISIBLE);
                        cbxMym.setVisibility(View.VISIBLE);
                        cbxTop.setVisibility(View.VISIBLE);
                        cbxCebolla.setVisibility(View.GONE);
                        cbxTomate.setVisibility(View.GONE);
                        cbxPepinillo.setVisibility(View.GONE);
                        cbxLeche.setVisibility(View.GONE);
                        cbxCafeina.setVisibility(View.GONE);
                        cbxAzucar.setVisibility(View.GONE);

                        break;
                    case 1:
                        cbxOreo.setVisibility(View.GONE);
                        cbxMym.setVisibility(View.GONE);
                        cbxTop.setVisibility(View.GONE);
                        cbxCebolla.setVisibility(View.GONE);
                        cbxTomate.setVisibility(View.GONE);
                        cbxPepinillo.setVisibility(View.GONE);
                        cbxLeche.setVisibility(View.VISIBLE);
                        cbxCafeina.setVisibility(View.VISIBLE);
                        cbxAzucar.setVisibility(View.VISIBLE);
                        break;
                    case 2:
                        cbxOreo.setVisibility(View.GONE);
                        cbxMym.setVisibility(View.GONE);
                        cbxTop.setVisibility(View.GONE);
                        cbxCebolla.setVisibility(View.VISIBLE);
                        cbxTomate.setVisibility(View.VISIBLE);
                        cbxPepinillo.setVisibility(View.VISIBLE);
                        cbxLeche.setVisibility(View.GONE);
                        cbxCafeina.setVisibility(View.GONE);
                        cbxAzucar.setVisibility(View.GONE);
                        break;

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void addNewElement(View view) {

        String sValue = sDetailOptions.getSelectedItem().toString();

        if (sValue.equalsIgnoreCase("McFlurry")) {
            lstCon = new ArrayList<>();
            lstSin = new ArrayList<>();
            mcTemp.setFoodType(FoodType.MC_FLURY);
            if (cbxOreo.isChecked()) {

                lstCon.add("Oreo");
            } else {

                lstSin.add("Oreo");
            }

            if (cbxTop.isChecked()) {

                lstCon.add("Toping");
            } else {

                lstSin.add("Toping");
            }

            if (cbxMym.isChecked()) {

                lstCon.add("MyM");
            } else {
                mcTemp.setFoodType(FoodType.MC_FLURY);
                lstSin.add("MyM");
            }

        } else if (sValue.equalsIgnoreCase("Cafe")) {
            lstCon = new ArrayList<>();
            lstSin = new ArrayList<>();

            mcTemp.setFoodType(FoodType.CAFE);
            if (cbxAzucar.isChecked()) {

                lstCon.add("Azucar");
            } else {

                lstSin.add("Azucar");
            }

            if (cbxLeche.isChecked()) {

                lstCon.add("Leche");
            } else {
                mcTemp.setFoodType(FoodType.CAFE);
                lstSin.add("Leche");
            }

            if (cbxCafeina.isChecked()) {

                lstCon.add("Cafeina");
            } else {

                lstSin.add("Cafeina");
            }

        } else {
            lstCon = new ArrayList<>();
            lstSin = new ArrayList<>();
            mcTemp.setFoodType(FoodType.HAMBURGUESA);
            if (cbxCebolla.isChecked()) {

                lstCon.add("Cebolla");
            } else {

                lstSin.add("Cebolla");
            }

            if (cbxTomate.isChecked()) {

                lstCon.add("Tomate");
            } else {

                lstSin.add("Tomate");
            }

            if (cbxPepinillo.isChecked()) {

                lstCon.add("Pepinillo");
            } else {

                lstSin.add("Pepinillo");

            }
        }
        mcTemp.setConList(lstCon);
        mcTemp.setSinList(lstSin);


        if ((mcTemp != null)) {
            Intent intent = new Intent();
            intent.putExtra(RecyclerActivity.RESULT_STR, mcTemp);
            setResult(RESULT_OK, intent);
            finish();
        }

    }
}
