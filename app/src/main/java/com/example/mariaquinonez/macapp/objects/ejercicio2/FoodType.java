package com.example.mariaquinonez.macapp.objects.ejercicio2;

import android.content.Context;

import com.example.mariaquinonez.macapp.R;

/**
 * Created by maria.quinonez on 02/11/2017.
 */

public enum FoodType {
    CAFE,
    MC_FLURY,
    HAMBURGUESA;

    public String toString(Context context){
        String[] options = context.getResources().getStringArray(R.array.orden_opt);

       switch (this){
           case CAFE:
               return context.getString(R.string.opt_cafe);
           case MC_FLURY:
               return context.getString(R.string.opt_mcflurry);
           case HAMBURGUESA:
               return context.getString(R.string.opt_hamburguer);
           default:
               return "";
       }
    }
    public int getImage(){
        switch (this) {
            case CAFE:
                return R.drawable.cafe;
            case MC_FLURY:
                return R.drawable.mcflurry;
            case HAMBURGUESA:
                return R.drawable.hamburguer;
            default:
                return R.drawable.box;

        }
    }
    public static FoodType getFoodType(int position){
        switch (position){
            case 0:
                return CAFE;
            case 1:
                return MC_FLURY;
            case 2:
                return  HAMBURGUESA;
            default:
                return  null;
        }
    }
}
