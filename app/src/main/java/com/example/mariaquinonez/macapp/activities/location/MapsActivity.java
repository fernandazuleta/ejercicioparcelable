package com.example.mariaquinonez.macapp.activities.location;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.LocaleList;
import android.os.Looper;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.example.mariaquinonez.macapp.R;
import com.example.mariaquinonez.macapp.activities.OptionsActivity;
import com.example.mariaquinonez.macapp.helpers.PermissionsManager;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private LocationManager locationManager;
    private LocationRequest locationRequest;
    private LocationCallback locationCallback;
    private FusedLocationProviderClient fusedLocationProviderClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //VERSION DEL sdk del telefono
        if(Build.VERSION.SDK_INT >22){
            PermissionsManager.requestFineLocation(this);

        }
        locationManager=(LocationManager)
                getSystemService(Context.LOCATION_SERVICE);
        if(!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)||
                !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)){
            new AlertDialog.Builder(this).setMessage("Tiene apagada las funciones de ubicacion, es" +
                    "necesario que las encienda").setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Intent intent=new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            }).create().show();

        }
        initComponents();
    }
    @SuppressLint("MissingPermission")
    @Override
    public void onStart(){
        super.onStart();
        fusedLocationProviderClient.requestLocationUpdates(locationRequest,locationCallback, Looper.myLooper());
    }

    @Override
    public void onStop(){
        super.onStop();
        fusedLocationProviderClient.removeLocationUpdates(locationCallback);
    }
    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }
    @Override public void onRequestPermissionsResult(
            int requestCode, String[] permissions, int[] grantResults){
        if(requestCode== PermissionsManager.PERMISSIONS_REQUEST){
            if(permissions.length>0){
                if(grantResults[0]== PackageManager.PERMISSION_DENIED){
                    new AlertDialog.Builder(this).setMessage("Es necesario tener los permisos de ubicacion").setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    }).setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            finish();
                        }
                    }).create().show();
                }
            }
        }
    }

    private void initComponents(){
                fusedLocationProviderClient= LocationServices.getFusedLocationProviderClient(this);
        SettingsClient settingsClient=LocationServices.getSettingsClient(this);
        locationCallback= new LocationCallback(){
            @Override
            public void onLocationResult(LocationResult result){
                Location location=result.getLocations().get(0);
                MarkerOptions option= new MarkerOptions();
                option.position(new LatLng(location.getLatitude(),
                        location.getLongitude()));
                BitmapDescriptor icon= BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN);
                option.icon(icon);

                Marker marker=mMap.addMarker(option);
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                        marker.getPosition(),17
                ));
            }
        };
        locationRequest= new LocationRequest();
        locationRequest.setInterval(10_000);
        locationRequest.setFastestInterval(5_000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);


    }
}
