package com.example.mariaquinonez.macapp.objects;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by maria.quinonez on 10/10/2017.
 */

public class Hamburguesa implements Parcelable {
    private   boolean cebolla;
    private   boolean tomate;
    private   boolean pepinillo;

    public Hamburguesa() {
    }

    public Hamburguesa(boolean cebolla, boolean tomate, boolean pepinillo) {
        this.cebolla = cebolla;
        this.tomate = tomate;
        this.pepinillo = pepinillo;
    }

    public boolean isCebolla() {
        return cebolla;
    }

    public void setCebolla(boolean cebolla) {
        this.cebolla = cebolla;
    }

    public boolean isTomate() {
        return tomate;
    }

    public void setTomate(boolean tomate) {
        this.tomate = tomate;
    }

    public boolean isPepinillo() {
        return pepinillo;
    }

    public void setPepinillo(boolean pepinillo) {
        this.pepinillo = pepinillo;
    }

    protected Hamburguesa(Parcel in) {
        cebolla = in.readByte() != 0x00;
        tomate = in.readByte() != 0x00;
        pepinillo = in.readByte() != 0x00;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (cebolla ? 0x01 : 0x00));
        dest.writeByte((byte) (tomate ? 0x01 : 0x00));
        dest.writeByte((byte) (pepinillo ? 0x01 : 0x00));
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Hamburguesa> CREATOR = new Parcelable.Creator<Hamburguesa>() {
        @Override
        public Hamburguesa createFromParcel(Parcel in) {
            return new Hamburguesa(in);
        }

        @Override
        public Hamburguesa[] newArray(int size) {
            return new Hamburguesa[size];
        }
    };
}