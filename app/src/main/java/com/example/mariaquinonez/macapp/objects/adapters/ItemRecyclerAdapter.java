package com.example.mariaquinonez.macapp.objects.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mariaquinonez.macapp.objects.ejercicio2.McOrden;
import com.example.mariaquinonez.macapp.objects.holders.ItemOrderHolder;

import java.util.List;

/**
 * Created by maria.quinonez on 14/11/2017.
 */

public class ItemRecyclerAdapter extends RecyclerView.Adapter<ItemOrderHolder> {

    private List<McOrden> mcOrdenList;
    private Activity activity;
    private int resourceLayout;

    public ItemRecyclerAdapter(List<McOrden> mcOrdenList, Activity activity, int resourceLayout) {
        this.mcOrdenList = mcOrdenList;
        this.activity = activity;
        this.resourceLayout = resourceLayout;
    }

    @Override
    public ItemOrderHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemOrderHolder(LayoutInflater.from(activity).inflate(resourceLayout,parent,false));
    }

    @Override
    public void onBindViewHolder(ItemOrderHolder holder, int position) {
    //renderiza el layout
        McOrden mcOrden=mcOrdenList.get(position);
        holder.setMcOrder(mcOrden);


        holder.getCvContainer().setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){

            }
        });
    }

    @Override
    public int getItemCount() {
        return mcOrdenList.size();
    }
}
