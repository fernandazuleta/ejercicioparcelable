package com.example.mariaquinonez.macapp.objects;
import android.os.Parcel;
import android.os.Parcelable;

public class McFlurry implements Parcelable {
    private boolean toping;
    private boolean oreo;
    private boolean mym;

    public McFlurry(boolean toping, boolean oreo, boolean mym) {
        this.toping = toping;
        this.oreo = oreo;
        this.mym = mym;
    }

    public McFlurry() {
    }

    public boolean isToping() {
        return toping;
    }

    public void setToping(boolean toping) {
        this.toping = toping;
    }

    public boolean isOreo() {
        return oreo;
    }

    public void setOreo(boolean oreo) {
        this.oreo = oreo;
    }

    public boolean isMym() {
        return mym;
    }

    public void setMym(boolean mym) {
        this.mym = mym;
    }

    protected McFlurry(Parcel in) {
        toping = in.readByte() != 0x00;
        oreo = in.readByte() != 0x00;
        mym = in.readByte() != 0x00;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (toping ? 0x01 : 0x00));
        dest.writeByte((byte) (oreo ? 0x01 : 0x00));
        dest.writeByte((byte) (mym ? 0x01 : 0x00));
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<McFlurry> CREATOR = new Parcelable.Creator<McFlurry>() {
        @Override
        public McFlurry createFromParcel(Parcel in) {
            return new McFlurry(in);
        }

        @Override
        public McFlurry[] newArray(int size) {
            return new McFlurry[size];
        }
    };
}