package com.example.mariaquinonez.macapp.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.example.mariaquinonez.macapp.R;
import com.example.mariaquinonez.macapp.objects.Cafe;
import com.example.mariaquinonez.macapp.objects.Hamburguesa;
import com.example.mariaquinonez.macapp.objects.McFlurry;
import com.example.mariaquinonez.macapp.objects.Orden2;

import java.util.List;

public class EjercicioParcelableActivity extends AppCompatActivity {
    private TextView tvNuevaOrden;
    private Orden2 nuevaOrden;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicio_parcelable);
        initComponents();
    }
    private void initComponents(){
        nuevaOrden=getIntent().getParcelableExtra("llave");
        tvNuevaOrden = (TextView) findViewById(R.id.tv_nueva_orden);
        List<Hamburguesa> hamburguesaList= nuevaOrden.getHamburguesaList();
        List<Cafe> cafeList = nuevaOrden.getCafeList();
        List<McFlurry> mcFlurryList=nuevaOrden.getMcFlurryList();

        String misPedidos="Hamburguesas";
        for(Hamburguesa hamburguesa: hamburguesaList){
            misPedidos+="\n   Pepinillos:"+getVal(hamburguesa.isPepinillo());
            misPedidos+="\n   Tomates:"+getVal(hamburguesa.isTomate());
            misPedidos+="\n   Cebolla:"+getVal(hamburguesa.isCebolla());

        }
        misPedidos+="McFlurryes";
        for(McFlurry mcFlurry: mcFlurryList) {
            misPedidos += "\n   MYM:" + getVal(mcFlurry.isMym());
            misPedidos += "\n   Oreo:" + getVal(mcFlurry.isOreo());
            misPedidos += "\n   Toping:" + getVal(mcFlurry.isToping());

        }
        misPedidos+="Cafe";
        for(Cafe cafe: cafeList) {
            misPedidos += "\n   Azucar:" + getVal(cafe.isAzucar());
            misPedidos += "\n   Cafeina:" + getVal(cafe.isCafeina());
            misPedidos += "\n   Leche:" + getVal(cafe.isLeche());

        }
        tvNuevaOrden.setText(misPedidos);

    }
    private String getVal(boolean bool){
        return ((bool)? "Si":"No");
    }
}
